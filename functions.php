<?php

/**
 * Le add action permet de déclencher une fonction à un moment donnée (c'est le premier
 * argument), le deuxième argument correspond au nom de la fonction
 */
add_action("customize_register", "customize_footer");

/**
 * Cette fonction nous permettra de définir des nouveaux champs dans notre customizer
 * (le truc quand on click sur "personnaliser")
 */
function customize_footer(WP_Customize_Manager $wp_customize) {
    /**
     * Ici on définit une nouvelle section du customizer, en lui donnant un titre et
     * un id (on choisit les deux)
     */
    $wp_customize->add_section("simplon_footer_section", [
        "title" => "Footer simplon"
    ]);
    /**
     * On définit, un setting/un paramètre/une variable qui sera stocké en bdd par
     * wordpress automatiquement et dont on pourra réutiliser n'importe où
     */
    $wp_customize->add_setting("simplon_footer_text", [
        "default" => "Proudly powered by Simplon."
    ]);
    /**
     * On définit un champ, ici de type de type text, qui sera lié au setting déclaré
     * juste au dessus, et on indique dans quelle section va se trouver ce champ, en 
     * lui donnant l'id de la section (simplon_footer_section)
     */
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, "simplon_footer_text", [
        "label" => "Footer text",
        "section" => "simplon_footer_section"
    ]));
}

add_action("customize_register", "customize_header");


function customize_header(WP_Customize_Manager $wp_customize) {
  
    $wp_customize->add_section("simplon_header_section", [
        "title" => "Header"
    ]);
    
    $wp_customize->add_setting("simplon_header_img");
    
    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, "simplon_header_img", [
        "label" => "Header image",
        "section" => "simplon_header_section",
        "mime_type" => "image"
    ]));
}



/**
 * Tout ça c'est pour faire en sorte d'appliquer le style.css de l'enfant
 */
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles()
{
    wp_enqueue_style(
        'child-style',
        get_stylesheet_uri(),
        [],
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
}