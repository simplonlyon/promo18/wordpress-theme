# Thème enfant

Dans ce projet, on fait un thème enfant pour wordpress dans lequel on rend modifiables certaines parties du thème et on en rajoute d'autres.

## How To Use
Cloner le projet directement dans le dossier `wp-content/themes` de votre wordpress puis dans l'administration de celui ci, activer le thème `Simplon Theme`


## Créer un thème enfant

Un thème enfant permet de modifier un thème existant pour, par exemple, rajouter des zones modifiables qui ne le sont pas par défaut, modifier la mise en page d'une section donnée, changer le css ou autre.

Cela permet de conserver toutes les fonctionnalités et apparences du thème parent et de ne modifier que celles que l'ont ciblera dans le thème enfant.

Les éléments nécessaires :

* Créer un dossier pour notre thème enfant dans le dossier `wp-content/themes`
* Créer un fichier [style.css](style.css) et dans celui ci définir sous forme de commentaire le nom du thème et le nom du dossier du thème parent
* Créer un fichier [functions.php](functions.php) qui contiendra les différents hooks pour rajouter les fonctionnalités / champs personnalisables / etc.
